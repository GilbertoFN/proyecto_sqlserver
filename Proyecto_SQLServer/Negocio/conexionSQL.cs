﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Objetos;

namespace Negocio
{
    public class conexionSQL
    {
        Conexion conexion = new Conexion();

        public List<ObjConsulta1> consulta1()
        {
            return conexion.consulta1();
        }

        public List<ObjConsulta2> consulta2()
        {
            return conexion.consulta2();
        }

        public List<ObjConsulta3> consulta3()
        {
            return conexion.consulta3();
        }

        public List<ObjConsulta4> consulta4()
        {
            return conexion.consulta4();
        }

        public List<ObjConsulta5> consulta5()
        {
            return conexion.consulta5();
        }

    }
}
