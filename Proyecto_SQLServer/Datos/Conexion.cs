﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Objetos;

namespace Datos
{
    public class Conexion
    {
        static string conexionstring = "server= localhost ; database = proyecto ; integrated security = true";
        SqlConnection conexion = new SqlConnection(conexionstring);
        ObjConsulta1 c1;
        ObjConsulta2 c2;
        ObjConsulta3 c3;
        ObjConsulta4 c4;
        ObjConsulta5 c5;

        public List<ObjConsulta1> consulta1()
        {
            List<ObjConsulta1> lista = new List<ObjConsulta1>();
            conexion.Open();
            string query = "SELECT count(*) as Total,pi.description,pa.name_contract_status FROM esquema_prestamos.personal_information as pi, esquema_application.application_train as ap, esquema_application.previous_application as pa WHERE ap.sk_id_curr = pa.sk_id_curr AND ap.id = pi.id_apptrain AND pi.valor != 'xna'GROUP BY pi.description,pa.name_contract_status";
            SqlCommand cmd = new SqlCommand(query, conexion);
            cmd.CommandTimeout = 240;
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    c1 = new ObjConsulta1
                    {
                        total = dr.GetInt32(0),
                        sexo = dr.GetString(1),
                        contrato = dr.GetString(2)
                    };
                    lista.Add(c1);
                }
            }
            conexion.Close();
            return lista;
        }

        public List<ObjConsulta2> consulta2()
        {
            List<ObjConsulta2> lista = new List<ObjConsulta2>();
            conexion.Open();
            string query = "SELECT count(*) as Total,valor,pa.name_contract_status FROM esquema_prestamos.personal_information as pi, esquema_application.application_train as ap, esquema_application.previous_application as pa WHERE ap.sk_id_curr = pa.sk_id_curr AND ap.id = pi.id_apptrain  AND(pi.valor = 'civil marriage' OR pi.valor = 'single')GROUP BY valor,pa.name_contract_status";
            SqlCommand cmd = new SqlCommand(query, conexion);
            cmd.CommandTimeout = 240;
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    c2 = new ObjConsulta2
                    {
                        total = dr.GetInt32(0),
                        estado_civil = dr.GetString(1),
                        contrato = dr.GetString(2)
                    };
                    lista.Add(c2);
                }
            }
            conexion.Close();
            return lista;
        }

        public List<ObjConsulta3> consulta3()
        {
            List<ObjConsulta3> lista = new List<ObjConsulta3>();
            conexion.Open();
            string query = "SELECT count(*) as Total,pi.valor,lpa.valor FROM esquema_prestamos.living_place_apptrain as lpa, esquema_application.application_train as ap, esquema_prestamos.personal_information as pi WHERE ap.id = lpa.id_apptrain AND ap.id = pi.id_apptrain AND pi.valor != 'xna' AND lpa.valor != 'null' GROUP BY pi.valor,lpa.valor";
            SqlCommand cmd = new SqlCommand(query, conexion);
            cmd.CommandTimeout = 240;
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    c3 = new ObjConsulta3
                    {
                        total = dr.GetInt32(0),
                        estado_civil = dr.GetString(1),
                        vivienda = dr.GetString(2)
                    };
                    lista.Add(c3);
                }
            }
            conexion.Close();
            return lista;
        }

        public List<ObjConsulta4> consulta4()
        {
            List<ObjConsulta4> lista = new List<ObjConsulta4>();
            conexion.Open();
            string query = "SELECT count(*) as Total,pi.description,fat.description FROM esquema_prestamos.personal_information as pi, esquema_application.application_train as ap, flag_apptrain as fat WHERE ap.id = fat.id_apptrain AND ap.id = pi.id_apptrain AND pi.valor != 'xna' AND(fat.description = 'flag_own_car' OR fat.description = 'xna') GROUP BY pi.description,fat.description";
            SqlCommand cmd = new SqlCommand(query, conexion);
            cmd.CommandTimeout = 240;
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    c4 = new ObjConsulta4
                    {
                        total = dr.GetInt32(0),
                        sexo = dr.GetString(1),
                        carro = dr.GetString(2)
                    };
                    lista.Add(c4);
                }
            }
            conexion.Close();
            return lista;
        }

        public List<ObjConsulta5> consulta5()
        {
            List<ObjConsulta5> lista = new List<ObjConsulta5>();
            conexion.Open();
            string query = "SELECT count(*) as Total,b.credit_active ,pi.valor FROM esquema_prestamos.personal_information as pi, esquema_application.application_train as ap, esquema_balance.bureau as b WHERE ap.sk_id_curr = b.sk_id_curr AND ap.id = pi.id_apptrain AND pi.valor != 'xna' GROUP BY b.credit_active,pi.valor";
            SqlCommand cmd = new SqlCommand(query, conexion);
            cmd.CommandTimeout = 240;
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    c5 = new ObjConsulta5
                    {
                        total = dr.GetInt32(0),
                        credit = dr.GetString(1),
                        estado_civil = dr.GetString(2)
                    };
                    lista.Add(c5);
                }
            }
            conexion.Close();
            return lista;
        }

    }
}
