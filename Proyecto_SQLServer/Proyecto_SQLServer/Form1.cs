﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Negocio;
using Objetos;
using System.Windows.Forms.DataVisualization.Charting;

namespace Proyecto_SQLServer
{
    public partial class Form1 : Form
    {
        conexionSQL conexion = new conexionSQL();        

        public Form1()
        {
            InitializeComponent();
        }

        private void cargarGraficos()
        {

            List<ObjConsulta1> lista = conexion.consulta1();
            int totalDatos = lista.Count();
            if (totalDatos == 7)
            {
                int total1 = lista[0].total;
                string sexo1 = lista[0].sexo;
                string contrato1 = lista[0].contrato;

                int total2 = lista[1].total;
                string sexo2 = lista[1].sexo;
                string contrato2 = lista[1].contrato;

                int total3 = lista[2].total;
                string sexo3 = lista[2].sexo;
                string contrato3 = lista[2].contrato;

                int total4 = lista[3].total;
                string sexo4 = lista[3].sexo;
                string contrato4 = lista[3].contrato;

                int total5 = lista[4].total;
                string sexo5 = lista[4].sexo;
                string contrato5 = lista[4].contrato;

                int total6 = lista[5].total;
                string sexo6 = lista[5].sexo;
                string contrato6 = lista[5].contrato;

                int total7 = lista[6].total;
                string sexo7 = lista[6].sexo;
                string contrato7 = lista[6].contrato;

                string[] seriesChart1 = { sexo1 + " - " + contrato1, sexo2 + " - " + contrato2, sexo3 + " - " + contrato3, sexo4 + " - " + contrato4, sexo5 + " - " + contrato5, sexo6 + " - " + contrato6, sexo7 + " - " + contrato7 };
                int[] puntosChart1 = { total1, total2, total3, total4, total5, total6, total7 };

                chart1.Titles.Add("Cantidad de hombres y mujeres, según el estado del contrato.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart1.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }
            else if (totalDatos == 8)
            {
                int total1 = lista[0].total;
                string sexo1 = lista[0].sexo;
                string contrato1 = lista[0].contrato;

                int total2 = lista[1].total;
                string sexo2 = lista[1].sexo;
                string contrato2 = lista[1].contrato;

                int total3 = lista[2].total;
                string sexo3 = lista[2].sexo;
                string contrato3 = lista[2].contrato;

                int total4 = lista[3].total;
                string sexo4 = lista[3].sexo;
                string contrato4 = lista[3].contrato;

                int total5 = lista[4].total;
                string sexo5 = lista[4].sexo;
                string contrato5 = lista[4].contrato;

                int total6 = lista[5].total;
                string sexo6 = lista[5].sexo;
                string contrato6 = lista[5].contrato;

                int total7 = lista[6].total;
                string sexo7 = lista[6].sexo;
                string contrato7 = lista[6].contrato;

                int total8 = lista[7].total;
                string sexo8 = lista[7].sexo;
                string contrato8 = lista[7].contrato;

                string[] seriesChart1 = { sexo1 + " - " + contrato1, sexo2 + " - " + contrato2, sexo3 + " - " + contrato3, sexo4 + " - " + contrato4, sexo5 + " - " + contrato5, sexo6 + " - " + contrato6, sexo7 + " - " + contrato7, sexo8 + " - " + contrato8 };
                int[] puntosChart1 = { total1, total2, total3, total4, total5, total6, total7, total8 };

                chart1.Titles.Add("Cantidad de hombres y mujeres, según el estado del contrato.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart1.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }
            else if (totalDatos == 9)
            {
                int total1 = lista[0].total;
                string sexo1 = lista[0].sexo;
                string contrato1 = lista[0].contrato;

                int total2 = lista[1].total;
                string sexo2 = lista[1].sexo;
                string contrato2 = lista[1].contrato;

                int total3 = lista[2].total;
                string sexo3 = lista[2].sexo;
                string contrato3 = lista[2].contrato;

                int total4 = lista[3].total;
                string sexo4 = lista[3].sexo;
                string contrato4 = lista[3].contrato;

                int total5 = lista[4].total;
                string sexo5 = lista[4].sexo;
                string contrato5 = lista[4].contrato;

                int total6 = lista[5].total;
                string sexo6 = lista[5].sexo;
                string contrato6 = lista[5].contrato;

                int total7 = lista[6].total;
                string sexo7 = lista[6].sexo;
                string contrato7 = lista[6].contrato;

                int total8 = lista[7].total;
                string sexo8 = lista[7].sexo;
                string contrato8 = lista[7].contrato;

                int total9 = lista[8].total;
                string sexo9 = lista[8].sexo;
                string contrato9 = lista[8].contrato;

                string[] seriesChart1 = { sexo1 + " - " + contrato1, sexo2 + " - " + contrato2, sexo3 + " - " + contrato3, sexo4 + " - " + contrato4, sexo5 + " - " + contrato5, sexo6 + " - " + contrato6, sexo7 + " - " + contrato7, sexo8 + " - " + contrato8, sexo9 + " - " + contrato9 };
                int[] puntosChart1 = { total1, total2, total3, total4, total5, total6, total7, total8, total9 };

                chart1.Titles.Add("Cantidad de hombres y mujeres, según el estado del contrato.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart1.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }
            else if (totalDatos == 10)
            {
                int total1 = lista[0].total;
                string sexo1 = lista[0].sexo;
                string contrato1 = lista[0].contrato;

                int total2 = lista[1].total;
                string sexo2 = lista[1].sexo;
                string contrato2 = lista[1].contrato;

                int total3 = lista[2].total;
                string sexo3 = lista[2].sexo;
                string contrato3 = lista[2].contrato;

                int total4 = lista[3].total;
                string sexo4 = lista[3].sexo;
                string contrato4 = lista[3].contrato;

                int total5 = lista[4].total;
                string sexo5 = lista[4].sexo;
                string contrato5 = lista[4].contrato;

                int total6 = lista[5].total;
                string sexo6 = lista[5].sexo;
                string contrato6 = lista[5].contrato;

                int total7 = lista[6].total;
                string sexo7 = lista[6].sexo;
                string contrato7 = lista[6].contrato;

                int total8 = lista[7].total;
                string sexo8 = lista[7].sexo;
                string contrato8 = lista[7].contrato;

                int total9 = lista[8].total;
                string sexo9 = lista[8].sexo;
                string contrato9 = lista[8].contrato;

                int total10 = lista[9].total;
                string sexo10 = lista[9].sexo;
                string contrato10 = lista[9].contrato;

                string[] seriesChart1 = { sexo1 + " - " + contrato1, sexo2 + " - " + contrato2, sexo3 + " - " + contrato3, sexo4 + " - " + contrato4, sexo5 + " - " + contrato5, sexo6 + " - " + contrato6, sexo7 + " - " + contrato7, sexo8 + " - " + contrato8, sexo9 + " - " + contrato9, sexo10 + " - " + contrato10 };
                int[] puntosChart1 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10 };

                chart1.Titles.Add("Cantidad de hombres y mujeres, según el estado del contrato.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart1.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }
            else if (totalDatos == 11)
            {
                int total1 = lista[0].total;
                string sexo1 = lista[0].sexo;
                string contrato1 = lista[0].contrato;

                int total2 = lista[1].total;
                string sexo2 = lista[1].sexo;
                string contrato2 = lista[1].contrato;

                int total3 = lista[2].total;
                string sexo3 = lista[2].sexo;
                string contrato3 = lista[2].contrato;

                int total4 = lista[3].total;
                string sexo4 = lista[3].sexo;
                string contrato4 = lista[3].contrato;

                int total5 = lista[4].total;
                string sexo5 = lista[4].sexo;
                string contrato5 = lista[4].contrato;

                int total6 = lista[5].total;
                string sexo6 = lista[5].sexo;
                string contrato6 = lista[5].contrato;

                int total7 = lista[6].total;
                string sexo7 = lista[6].sexo;
                string contrato7 = lista[6].contrato;

                int total8 = lista[7].total;
                string sexo8 = lista[7].sexo;
                string contrato8 = lista[7].contrato;

                int total9 = lista[8].total;
                string sexo9 = lista[8].sexo;
                string contrato9 = lista[8].contrato;

                int total10 = lista[9].total;
                string sexo10 = lista[9].sexo;
                string contrato10 = lista[9].contrato;


                int total11 = lista[10].total;
                string sexo11 = lista[10].sexo;
                string contrato11 = lista[10].contrato;

                string[] seriesChart1 = { sexo1 + " - " + contrato1, sexo2 + " - " + contrato2, sexo3 + " - " + contrato3, sexo4 + " - " + contrato4, sexo5 + " - " + contrato5, sexo6 + " - " + contrato6, sexo7 + " - " + contrato7, sexo8 + " - " + contrato8, sexo9 + " - " + contrato9, sexo10 + " - " + contrato10, sexo11 + " - " + contrato11 };
                int[] puntosChart1 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10, total11 };

                chart1.Titles.Add("Cantidad de hombres y mujeres, según el estado del contrato.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart1.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }

            List<ObjConsulta2> lista2 = conexion.consulta2();
            int totalDatos2 = lista2.Count();
            if (totalDatos2 == 7)
            {
                int total1 = lista2[0].total;
                string estado_civil1 = lista2[0].estado_civil;
                string contrato1 = lista2[0].contrato;

                int total2 = lista2[1].total;
                string estado_civil2 = lista2[1].estado_civil;
                string contrato2 = lista2[1].contrato;

                int total3 = lista2[2].total;
                string estado_civil3 = lista2[2].estado_civil;
                string contrato3 = lista2[2].contrato;

                int total4 = lista2[3].total;
                string estado_civil4 = lista2[3].estado_civil;
                string contrato4 = lista2[3].contrato;

                int total5 = lista2[4].total;
                string estado_civil5 = lista2[4].estado_civil;
                string contrato5 = lista2[4].contrato;

                int total6 = lista2[5].total;
                string estado_civil6 = lista2[5].estado_civil;
                string contrato6 = lista2[5].contrato;

                int total7 = lista2[6].total;
                string estado_civil7 = lista2[6].estado_civil;
                string contrato7 = lista2[6].contrato;

                string[] seriesChart1 = { estado_civil1 + " - " + contrato1, estado_civil2 + " - " + contrato2, estado_civil3 + " - " + contrato3, estado_civil4 + " - " + contrato4, estado_civil5 + " - " + contrato5, estado_civil6 + " - " + contrato6, estado_civil7 + " - " + contrato7 };
                int[] puntosChart1 = { total1, total2, total3, total4, total5, total6, total7 };

                chart2.Titles.Add("Cantidad de casados y solteros, según el estado del contrato.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart2.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }
            else if (totalDatos2 == 8)
            {
                int total1 = lista2[0].total;
                string estado_civil1 = lista2[0].estado_civil;
                string contrato1 = lista2[0].contrato;

                int total2 = lista2[1].total;
                string estado_civil2 = lista2[1].estado_civil;
                string contrato2 = lista2[1].contrato;

                int total3 = lista2[2].total;
                string estado_civil3 = lista2[2].estado_civil;
                string contrato3 = lista2[2].contrato;

                int total4 = lista2[3].total;
                string estado_civil4 = lista2[3].estado_civil;
                string contrato4 = lista2[3].contrato;

                int total5 = lista2[4].total;
                string estado_civil5 = lista2[4].estado_civil;
                string contrato5 = lista2[4].contrato;

                int total6 = lista2[5].total;
                string estado_civil6 = lista2[5].estado_civil;
                string contrato6 = lista2[5].contrato;

                int total7 = lista2[6].total;
                string estado_civil7 = lista2[6].estado_civil;
                string contrato7 = lista2[6].contrato;

                int total8 = lista2[7].total;
                string estado_civil8 = lista2[7].estado_civil;
                string contrato8 = lista2[7].contrato;

                string[] seriesChart1 = { estado_civil1 + " - " + contrato1, estado_civil2 + " - " + contrato2, estado_civil3 + " - " + contrato3, estado_civil4 + " - " + contrato4, estado_civil5 + " - " + contrato5, estado_civil6 + " - " + contrato6, estado_civil7 + " - " + contrato7, estado_civil8 + " - " + contrato8 };
                int[] puntosChart1 = { total1, total2, total3, total4, total5, total6, total7, total8 };

                chart2.Titles.Add("Cantidad de casados y solteros, según el estado del contrato.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart2.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }
            else if (totalDatos2 == 9)
            {
                int total1 = lista2[0].total;
                string estado_civil1 = lista2[0].estado_civil;
                string contrato1 = lista2[0].contrato;

                int total2 = lista2[1].total;
                string estado_civil2 = lista2[1].estado_civil;
                string contrato2 = lista2[1].contrato;

                int total3 = lista2[2].total;
                string estado_civil3 = lista2[2].estado_civil;
                string contrato3 = lista2[2].contrato;

                int total4 = lista2[3].total;
                string estado_civil4 = lista2[3].estado_civil;
                string contrato4 = lista2[3].contrato;

                int total5 = lista2[4].total;
                string estado_civil5 = lista2[4].estado_civil;
                string contrato5 = lista2[4].contrato;

                int total6 = lista2[5].total;
                string estado_civil6 = lista2[5].estado_civil;
                string contrato6 = lista2[5].contrato;

                int total7 = lista2[6].total;
                string estado_civil7 = lista2[6].estado_civil;
                string contrato7 = lista2[6].contrato;

                int total8 = lista2[7].total;
                string estado_civil8 = lista2[7].estado_civil;
                string contrato8 = lista2[7].contrato;

                int total9 = lista2[8].total;
                string estado_civil9 = lista2[8].estado_civil;
                string contrato9 = lista2[8].contrato;

                string[] seriesChart1 = { estado_civil1 + " - " + contrato1, estado_civil2 + " - " + contrato2, estado_civil3 + " - " + contrato3, estado_civil4 + " - " + contrato4, estado_civil5 + " - " + contrato5, estado_civil6 + " - " + contrato6, estado_civil7 + " - " + contrato7, estado_civil8 + " - " + contrato8, estado_civil9 + " - " + contrato9 };
                int[] puntosChart1 = { total1, total2, total3, total4, total5, total6, total7, total8, total9 };

                chart2.Titles.Add("Cantidad de casados y solteros, según el estado del contrato.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart2.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }
            else if (totalDatos2 == 10)
            {
                int total1 = lista2[0].total;
                string estado_civil1 = lista2[0].estado_civil;
                string contrato1 = lista2[0].contrato;

                int total2 = lista2[1].total;
                string estado_civil2 = lista2[1].estado_civil;
                string contrato2 = lista2[1].contrato;

                int total3 = lista2[2].total;
                string estado_civil3 = lista2[2].estado_civil;
                string contrato3 = lista2[2].contrato;

                int total4 = lista2[3].total;
                string estado_civil4 = lista2[3].estado_civil;
                string contrato4 = lista2[3].contrato;

                int total5 = lista2[4].total;
                string estado_civil5 = lista2[4].estado_civil;
                string contrato5 = lista2[4].contrato;

                int total6 = lista2[5].total;
                string estado_civil6 = lista2[5].estado_civil;
                string contrato6 = lista2[5].contrato;

                int total7 = lista2[6].total;
                string estado_civil7 = lista2[6].estado_civil;
                string contrato7 = lista2[6].contrato;

                int total8 = lista2[7].total;
                string estado_civil8 = lista2[7].estado_civil;
                string contrato8 = lista2[7].contrato;

                int total9 = lista2[8].total;
                string estado_civil9 = lista2[8].estado_civil;
                string contrato9 = lista2[8].contrato;

                int total10 = lista2[9].total;
                string estado_civil10 = lista2[9].estado_civil;
                string contrato10 = lista2[9].contrato;

                string[] seriesChart1 = { estado_civil1 + " - " + contrato1, estado_civil2 + " - " + contrato2, estado_civil3 + " - " + contrato3, estado_civil4 + " - " + contrato4, estado_civil5 + " - " + contrato5, estado_civil6 + " - " + contrato6, estado_civil7 + " - " + contrato7, estado_civil8 + " - " + contrato8, estado_civil9 + " - " + contrato9, estado_civil10 + " - " + contrato10 };
                int[] puntosChart1 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10 };

                chart2.Titles.Add("Cantidad de casados y solteros, según el estado del contrato.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart2.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }
            else if (totalDatos2 == 11)
            {
                int total1 = lista2[0].total;
                string estado_civil1 = lista2[0].estado_civil;
                string contrato1 = lista2[0].contrato;

                int total2 = lista2[1].total;
                string estado_civil2 = lista2[1].estado_civil;
                string contrato2 = lista2[1].contrato;

                int total3 = lista2[2].total;
                string estado_civil3 = lista2[2].estado_civil;
                string contrato3 = lista2[2].contrato;

                int total4 = lista2[3].total;
                string estado_civil4 = lista2[3].estado_civil;
                string contrato4 = lista2[3].contrato;

                int total5 = lista2[4].total;
                string estado_civil5 = lista2[4].estado_civil;
                string contrato5 = lista2[4].contrato;

                int total6 = lista2[5].total;
                string estado_civil6 = lista2[5].estado_civil;
                string contrato6 = lista2[5].contrato;

                int total7 = lista2[6].total;
                string estado_civil7 = lista2[6].estado_civil;
                string contrato7 = lista2[6].contrato;

                int total8 = lista2[7].total;
                string estado_civil8 = lista2[7].estado_civil;
                string contrato8 = lista2[7].contrato;

                int total9 = lista2[8].total;
                string estado_civil9 = lista2[8].estado_civil;
                string contrato9 = lista2[8].contrato;

                int total10 = lista2[9].total;
                string estado_civil10 = lista2[9].estado_civil;
                string contrato10 = lista2[9].contrato;


                int total11 = lista2[10].total;
                string estado_civil11 = lista2[10].estado_civil;
                string contrato11 = lista2[10].contrato;

                string[] seriesChart1 = { estado_civil1 + " - " + contrato1, estado_civil2 + " - " + contrato2, estado_civil3 + " - " + contrato3, estado_civil4 + " - " + contrato4, estado_civil5 + " - " + contrato5, estado_civil6 + " - " + contrato6, estado_civil7 + " - " + contrato7, estado_civil8 + " - " + contrato8, estado_civil9 + " - " + contrato9, estado_civil10 + " - " + contrato10, estado_civil11 + " - " + contrato11 };
                int[] puntosChart1 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10, total11 };

                chart2.Titles.Add("Cantidad de casados y solteros, según el estado del contrato.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart2.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }

            List<ObjConsulta3> lista3 = conexion.consulta3();
            int totalDatos3 = lista3.Count();
            if (totalDatos3 == 15)
            {
                int total1 = lista3[0].total;
                string estado_civil1 = lista3[0].estado_civil;
                string vivienda1 = lista3[0].vivienda;

                int total2 = lista3[1].total;
                string estado_civil2 = lista3[1].estado_civil;
                string vivienda2 = lista3[1].vivienda;

                int total3 = lista3[2].total;
                string estado_civil3 = lista3[2].estado_civil;
                string vivienda3 = lista3[2].vivienda;

                int total4 = lista3[3].total;
                string estado_civil4 = lista3[3].estado_civil;
                string vivienda4 = lista3[3].vivienda;

                int total5 = lista3[4].total;
                string estado_civil5 = lista3[4].estado_civil;
                string vivienda5 = lista3[4].vivienda;

                int total6 = lista3[5].total;
                string estado_civil6 = lista3[5].estado_civil;
                string vivienda6 = lista3[5].vivienda;

                int total7 = lista3[6].total;
                string estado_civil7 = lista3[6].estado_civil;
                string vivienda7 = lista3[6].vivienda;

                int total8 = lista3[7].total;
                string estado_civil8 = lista3[7].estado_civil;
                string vivienda8 = lista3[7].vivienda;

                int total9 = lista3[8].total;
                string estado_civil9 = lista3[8].estado_civil;
                string vivienda9 = lista3[8].vivienda;

                int total10 = lista3[9].total;
                string estado_civil10 = lista3[9].estado_civil;
                string vivienda10 = lista3[9].vivienda;

                int total11 = lista3[10].total;
                string estado_civil11 = lista3[10].estado_civil;
                string vivienda11 = lista3[10].vivienda;

                int total12 = lista3[11].total;
                string estado_civil12 = lista3[11].estado_civil;
                string vivienda12 = lista3[11].vivienda;

                int total13 = lista3[12].total;
                string estado_civil13 = lista3[12].estado_civil;
                string vivienda13 = lista3[12].vivienda;

                int total14 = lista3[13].total;
                string estado_civil14 = lista3[13].estado_civil;
                string vivienda14 = lista3[13].vivienda;

                int total15 = lista3[14].total;
                string estado_civil15 = lista3[14].estado_civil;
                string vivienda15 = lista3[14].vivienda;

                string[] seriesChart3 = { estado_civil1 + "|" + vivienda1 + " = " + total1, estado_civil2 + "|" + vivienda2 + " = " + total2, estado_civil3 + "|" + vivienda3 + " = " + total3, estado_civil4 + "|" + vivienda4 + " = " + total4, estado_civil5 + "|" + vivienda5 + " = " + total5, estado_civil6 + "|" + vivienda6 + " = " + total6, estado_civil7 + "|" + vivienda7 + " = " + total7, estado_civil8 + "|" + vivienda8 + " = " + total8, estado_civil9 + "|" + vivienda9 + " = " + total9, estado_civil10 + "|" + vivienda10 + " = " + total10, estado_civil11 + "|" + vivienda11 + " = " + total11, estado_civil12 + "" + vivienda12 + " = " + total12, estado_civil13 + "|" + vivienda13 + " = " + total13, estado_civil14 + " - " + vivienda14, estado_civil15 + "|" + vivienda15 + " = " + total15 };
                int[] puntosChart3 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10, total11, total12, total13, total14, total15 };

                chart3.Titles.Add("Cantidad de personas según con quien viven y la información de donde viven.");

                chart3.Series.Add(seriesChart3[0]);
                chart3.Series.Add(seriesChart3[1]);
                chart3.Series.Add(seriesChart3[2]);
                chart3.Series.Add(seriesChart3[3]);
                chart3.Series.Add(seriesChart3[4]);
                chart3.Series.Add(seriesChart3[5]);
                chart3.Series.Add(seriesChart3[6]);
                chart3.Series.Add(seriesChart3[7]);
                chart3.Series.Add(seriesChart3[8]);
                chart3.Series.Add(seriesChart3[9]);
                chart3.Series.Add(seriesChart3[10]);
                chart3.Series.Add(seriesChart3[11]);
                chart3.Series.Add(seriesChart3[12]);
                chart3.Series.Add(seriesChart3[13]);
                chart3.Series.Add(seriesChart3[14]);

                chart3.Series[0].Points.Add(puntosChart3[0]);
                chart3.Series[1].Points.Add(puntosChart3[1]);
                chart3.Series[2].Points.Add(puntosChart3[2]);
                chart3.Series[3].Points.Add(puntosChart3[3]);
                chart3.Series[4].Points.Add(puntosChart3[4]);
                chart3.Series[5].Points.Add(puntosChart3[5]);
                chart3.Series[6].Points.Add(puntosChart3[6]);
                chart3.Series[7].Points.Add(puntosChart3[7]);
                chart3.Series[8].Points.Add(puntosChart3[8]);
                chart3.Series[9].Points.Add(puntosChart3[9]);
                chart3.Series[10].Points.Add(puntosChart3[10]);
                chart3.Series[11].Points.Add(puntosChart3[11]);
                chart3.Series[12].Points.Add(puntosChart3[12]);
                chart3.Series[13].Points.Add(puntosChart3[13]);
                chart3.Series[14].Points.Add(puntosChart3[14]);

                Color[] colores = new Color[] { Color.DarkOrange };
                chart3.Palette = ChartColorPalette.EarthTones;


                //for (int i = 0; i < seriesChart1.Length; i++)
                //{
                //    chart3.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                //}
            }
            else if (totalDatos3 == 16)
            {
                int total1 = lista3[0].total;
                string estado_civil1 = lista3[0].estado_civil;
                string vivienda1 = lista3[0].vivienda;

                int total2 = lista3[1].total;
                string estado_civil2 = lista3[1].estado_civil;
                string vivienda2 = lista3[1].vivienda;

                int total3 = lista3[2].total;
                string estado_civil3 = lista3[2].estado_civil;
                string vivienda3 = lista3[2].vivienda;

                int total4 = lista3[3].total;
                string estado_civil4 = lista3[3].estado_civil;
                string vivienda4 = lista3[3].vivienda;

                int total5 = lista3[4].total;
                string estado_civil5 = lista3[4].estado_civil;
                string vivienda5 = lista3[4].vivienda;

                int total6 = lista3[5].total;
                string estado_civil6 = lista3[5].estado_civil;
                string vivienda6 = lista3[5].vivienda;

                int total7 = lista3[6].total;
                string estado_civil7 = lista3[6].estado_civil;
                string vivienda7 = lista3[6].vivienda;

                int total8 = lista3[7].total;
                string estado_civil8 = lista3[7].estado_civil;
                string vivienda8 = lista3[7].vivienda;

                int total9 = lista3[8].total;
                string estado_civil9 = lista3[8].estado_civil;
                string vivienda9 = lista3[8].vivienda;

                int total10 = lista3[9].total;
                string estado_civil10 = lista3[9].estado_civil;
                string vivienda10 = lista3[9].vivienda;

                int total11 = lista3[10].total;
                string estado_civil11 = lista3[10].estado_civil;
                string vivienda11 = lista3[10].vivienda;

                int total12 = lista3[11].total;
                string estado_civil12 = lista3[11].estado_civil;
                string vivienda12 = lista3[11].vivienda;

                int total13 = lista3[12].total;
                string estado_civil13 = lista3[12].estado_civil;
                string vivienda13 = lista3[12].vivienda;

                int total14 = lista3[13].total;
                string estado_civil14 = lista3[13].estado_civil;
                string vivienda14 = lista3[13].vivienda;

                int total15 = lista3[14].total;
                string estado_civil15 = lista3[14].estado_civil;
                string vivienda15 = lista3[14].vivienda;

                int total16 = lista3[15].total;
                string estado_civil16 = lista3[15].estado_civil;
                string vivienda16 = lista3[15].vivienda;

                string[] seriesChart3 = { estado_civil1 + "|" + vivienda1+" = "+total1, estado_civil2 + "|" + vivienda2 + " = " + total2, estado_civil3 + "|" + vivienda3 + " = " + total3, estado_civil4 + "|" + vivienda4 + " = " + total4, estado_civil5 + "|" + vivienda5 + " = " + total5, estado_civil6 + "|" + vivienda6 + " = " + total6, estado_civil7 + "|" + vivienda7 + " = " + total7, estado_civil8 + "|" + vivienda8 + " = " + total8, estado_civil9 + "|" + vivienda9 + " = " + total9, estado_civil10 + "|" + vivienda10 + " = " + total10, estado_civil11 + "|" + vivienda11 + " = " + total11, estado_civil12 + "" + vivienda12 + " = " + total12, estado_civil13 + "|" + vivienda13 + " = " + total13, estado_civil14 + " - " + vivienda14, estado_civil15 + "|" + vivienda15 + " = " + total15, estado_civil16 + "|" + vivienda16 + " = " + total16 };
                int[] puntosChart3 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10, total11, total12, total13, total14, total15, total16 };

                chart3.Titles.Add("Cantidad de personas según con quien viven y la información de donde viven.");

                chart3.Series.Add(seriesChart3[0]);
                chart3.Series.Add(seriesChart3[1]);
                chart3.Series.Add(seriesChart3[2]);
                chart3.Series.Add(seriesChart3[3]);
                chart3.Series.Add(seriesChart3[4]);
                chart3.Series.Add(seriesChart3[5]);
                chart3.Series.Add(seriesChart3[6]);
                chart3.Series.Add(seriesChart3[7]);
                chart3.Series.Add(seriesChart3[8]);
                chart3.Series.Add(seriesChart3[9]);
                chart3.Series.Add(seriesChart3[10]);
                chart3.Series.Add(seriesChart3[11]);
                chart3.Series.Add(seriesChart3[12]);
                chart3.Series.Add(seriesChart3[13]);
                chart3.Series.Add(seriesChart3[14]);
                chart3.Series.Add(seriesChart3[15]);

                chart3.Series[0].Points.Add(puntosChart3[0]);
                chart3.Series[1].Points.Add(puntosChart3[1]);
                chart3.Series[2].Points.Add(puntosChart3[2]);
                chart3.Series[3].Points.Add(puntosChart3[3]);
                chart3.Series[4].Points.Add(puntosChart3[4]);
                chart3.Series[5].Points.Add(puntosChart3[5]);
                chart3.Series[6].Points.Add(puntosChart3[6]);
                chart3.Series[7].Points.Add(puntosChart3[7]);
                chart3.Series[8].Points.Add(puntosChart3[8]);
                chart3.Series[9].Points.Add(puntosChart3[9]);
                chart3.Series[10].Points.Add(puntosChart3[10]);
                chart3.Series[11].Points.Add(puntosChart3[11]);
                chart3.Series[12].Points.Add(puntosChart3[12]);
                chart3.Series[13].Points.Add(puntosChart3[13]);
                chart3.Series[14].Points.Add(puntosChart3[14]);
                chart3.Series[15].Points.Add(puntosChart3[15]);                

                Color[] colores = new Color[] { Color.DarkOrange };
                chart3.Palette = ChartColorPalette.EarthTones;

                //chart3.PaletteCustomColors = colores;

                //for (int i = 0; i < seriesChart1.Length; i++)
                //{
                //    chart3.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                //}
            }
            else if (totalDatos3 == 17)
            {
                int total1 = lista3[0].total;
                string estado_civil1 = lista3[0].estado_civil;
                string vivienda1 = lista3[0].vivienda;

                int total2 = lista3[1].total;
                string estado_civil2 = lista3[1].estado_civil;
                string vivienda2 = lista3[1].vivienda;

                int total3 = lista3[2].total;
                string estado_civil3 = lista3[2].estado_civil;
                string vivienda3 = lista3[2].vivienda;

                int total4 = lista3[3].total;
                string estado_civil4 = lista3[3].estado_civil;
                string vivienda4 = lista3[3].vivienda;

                int total5 = lista3[4].total;
                string estado_civil5 = lista3[4].estado_civil;
                string vivienda5 = lista3[4].vivienda;

                int total6 = lista3[5].total;
                string estado_civil6 = lista3[5].estado_civil;
                string vivienda6 = lista3[5].vivienda;

                int total7 = lista3[6].total;
                string estado_civil7 = lista3[6].estado_civil;
                string vivienda7 = lista3[6].vivienda;

                int total8 = lista3[7].total;
                string estado_civil8 = lista3[7].estado_civil;
                string vivienda8 = lista3[7].vivienda;

                int total9 = lista3[8].total;
                string estado_civil9 = lista3[8].estado_civil;
                string vivienda9 = lista3[8].vivienda;

                int total10 = lista3[9].total;
                string estado_civil10 = lista3[9].estado_civil;
                string vivienda10 = lista3[9].vivienda;

                int total11 = lista3[10].total;
                string estado_civil11 = lista3[10].estado_civil;
                string vivienda11 = lista3[10].vivienda;

                int total12 = lista3[11].total;
                string estado_civil12 = lista3[11].estado_civil;
                string vivienda12 = lista3[11].vivienda;

                int total13 = lista3[12].total;
                string estado_civil13 = lista3[12].estado_civil;
                string vivienda13 = lista3[12].vivienda;

                int total14 = lista3[13].total;
                string estado_civil14 = lista3[13].estado_civil;
                string vivienda14 = lista3[13].vivienda;

                int total15 = lista3[14].total;
                string estado_civil15 = lista3[14].estado_civil;
                string vivienda15 = lista3[14].vivienda;

                int total16 = lista3[15].total;
                string estado_civil16 = lista3[15].estado_civil;
                string vivienda16 = lista3[15].vivienda;

                int total17 = lista3[16].total;
                string estado_civil17 = lista3[16].estado_civil;
                string vivienda17 = lista3[16].vivienda;

                string[] seriesChart3 = { estado_civil1 + "|" + vivienda1 + " = " + total1, estado_civil2 + "|" + vivienda2 + " = " + total2, estado_civil3 + "|" + vivienda3 + " = " + total3, estado_civil4 + "|" + vivienda4 + " = " + total4, estado_civil5 + "|" + vivienda5 + " = " + total5, estado_civil6 + "|" + vivienda6 + " = " + total6, estado_civil7 + "|" + vivienda7 + " = " + total7, estado_civil8 + "|" + vivienda8 + " = " + total8, estado_civil9 + "|" + vivienda9 + " = " + total9, estado_civil10 + "|" + vivienda10 + " = " + total10, estado_civil11 + "|" + vivienda11 + " = " + total11, estado_civil12 + "" + vivienda12 + " = " + total12, estado_civil13 + "|" + vivienda13 + " = " + total13, estado_civil14 + " - " + vivienda14, estado_civil15 + "|" + vivienda15 + " = " + total15, estado_civil16 + "|" + vivienda16 + " = " + total16, estado_civil17 + "|" + vivienda17 + " = " + total17 };
                int[] puntosChart3 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10, total11, total12, total13, total14, total15, total16,total17 };

                chart3.Titles.Add("Cantidad de personas según con quien viven y la información de donde viven.");

                chart3.Series.Add(seriesChart3[0]);
                chart3.Series.Add(seriesChart3[1]);
                chart3.Series.Add(seriesChart3[2]);
                chart3.Series.Add(seriesChart3[3]);
                chart3.Series.Add(seriesChart3[4]);
                chart3.Series.Add(seriesChart3[5]);
                chart3.Series.Add(seriesChart3[6]);
                chart3.Series.Add(seriesChart3[7]);
                chart3.Series.Add(seriesChart3[8]);
                chart3.Series.Add(seriesChart3[9]);
                chart3.Series.Add(seriesChart3[10]);
                chart3.Series.Add(seriesChart3[11]);
                chart3.Series.Add(seriesChart3[12]);
                chart3.Series.Add(seriesChart3[13]);
                chart3.Series.Add(seriesChart3[14]);
                chart3.Series.Add(seriesChart3[15]);
                chart3.Series.Add(seriesChart3[16]);

                chart3.Series[0].Points.Add(puntosChart3[0]);
                chart3.Series[1].Points.Add(puntosChart3[1]);
                chart3.Series[2].Points.Add(puntosChart3[2]);
                chart3.Series[3].Points.Add(puntosChart3[3]);
                chart3.Series[4].Points.Add(puntosChart3[4]);
                chart3.Series[5].Points.Add(puntosChart3[5]);
                chart3.Series[6].Points.Add(puntosChart3[6]);
                chart3.Series[7].Points.Add(puntosChart3[7]);
                chart3.Series[8].Points.Add(puntosChart3[8]);
                chart3.Series[9].Points.Add(puntosChart3[9]);
                chart3.Series[10].Points.Add(puntosChart3[10]);
                chart3.Series[11].Points.Add(puntosChart3[11]);
                chart3.Series[12].Points.Add(puntosChart3[12]);
                chart3.Series[13].Points.Add(puntosChart3[13]);
                chart3.Series[14].Points.Add(puntosChart3[14]);
                chart3.Series[15].Points.Add(puntosChart3[15]);
                chart3.Series[16].Points.Add(puntosChart3[16]);

                Color[] colores = new Color[] { Color.DarkOrange };
                chart3.Palette = ChartColorPalette.EarthTones;
            }
            else if (totalDatos3 == 18)
            {
                int total1 = lista3[0].total;
                string estado_civil1 = lista3[0].estado_civil;
                string vivienda1 = lista3[0].vivienda;

                int total2 = lista3[1].total;
                string estado_civil2 = lista3[1].estado_civil;
                string vivienda2 = lista3[1].vivienda;

                int total3 = lista3[2].total;
                string estado_civil3 = lista3[2].estado_civil;
                string vivienda3 = lista3[2].vivienda;

                int total4 = lista3[3].total;
                string estado_civil4 = lista3[3].estado_civil;
                string vivienda4 = lista3[3].vivienda;

                int total5 = lista3[4].total;
                string estado_civil5 = lista3[4].estado_civil;
                string vivienda5 = lista3[4].vivienda;

                int total6 = lista3[5].total;
                string estado_civil6 = lista3[5].estado_civil;
                string vivienda6 = lista3[5].vivienda;

                int total7 = lista3[6].total;
                string estado_civil7 = lista3[6].estado_civil;
                string vivienda7 = lista3[6].vivienda;

                int total8 = lista3[7].total;
                string estado_civil8 = lista3[7].estado_civil;
                string vivienda8 = lista3[7].vivienda;

                int total9 = lista3[8].total;
                string estado_civil9 = lista3[8].estado_civil;
                string vivienda9 = lista3[8].vivienda;

                int total10 = lista3[9].total;
                string estado_civil10 = lista3[9].estado_civil;
                string vivienda10 = lista3[9].vivienda;

                int total11 = lista3[10].total;
                string estado_civil11 = lista3[10].estado_civil;
                string vivienda11 = lista3[10].vivienda;

                int total12 = lista3[11].total;
                string estado_civil12 = lista3[11].estado_civil;
                string vivienda12 = lista3[11].vivienda;

                int total13 = lista3[12].total;
                string estado_civil13 = lista3[12].estado_civil;
                string vivienda13 = lista3[12].vivienda;

                int total14 = lista3[13].total;
                string estado_civil14 = lista3[13].estado_civil;
                string vivienda14 = lista3[13].vivienda;

                int total15 = lista3[14].total;
                string estado_civil15 = lista3[14].estado_civil;
                string vivienda15 = lista3[14].vivienda;

                int total16 = lista3[15].total;
                string estado_civil16 = lista3[15].estado_civil;
                string vivienda16 = lista3[15].vivienda;

                int total17 = lista3[16].total;
                string estado_civil17 = lista3[16].estado_civil;
                string vivienda17 = lista3[16].vivienda;

                int total18 = lista3[17].total;
                string estado_civil18 = lista3[17].estado_civil;
                string vivienda18 = lista3[17].vivienda;

                string[] seriesChart3 = { estado_civil1 + "|" + vivienda1 + " = " + total1, estado_civil2 + "|" + vivienda2 + " = " + total2, estado_civil3 + "|" + vivienda3 + " = " + total3, estado_civil4 + "|" + vivienda4 + " = " + total4, estado_civil5 + "|" + vivienda5 + " = " + total5, estado_civil6 + "|" + vivienda6 + " = " + total6, estado_civil7 + "|" + vivienda7 + " = " + total7, estado_civil8 + "|" + vivienda8 + " = " + total8, estado_civil9 + "|" + vivienda9 + " = " + total9, estado_civil10 + "|" + vivienda10 + " = " + total10, estado_civil11 + "|" + vivienda11 + " = " + total11, estado_civil12 + "" + vivienda12 + " = " + total12, estado_civil13 + "|" + vivienda13 + " = " + total13, estado_civil14 + " - " + vivienda14, estado_civil15 + "|" + vivienda15 + " = " + total15, estado_civil16 + "|" + vivienda16 + " = " + total16, estado_civil17 + "|" + vivienda17 + " = " + total17, estado_civil18 + "|" + vivienda18 + " = " + total18 };
                int[] puntosChart3 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10, total11, total12, total13, total14, total15, total16, total17,total18 };

                chart3.Titles.Add("Cantidad de personas según con quien viven y la información de donde viven.");

                chart3.Series.Add(seriesChart3[0]);
                chart3.Series.Add(seriesChart3[1]);
                chart3.Series.Add(seriesChart3[2]);
                chart3.Series.Add(seriesChart3[3]);
                chart3.Series.Add(seriesChart3[4]);
                chart3.Series.Add(seriesChart3[5]);
                chart3.Series.Add(seriesChart3[6]);
                chart3.Series.Add(seriesChart3[7]);
                chart3.Series.Add(seriesChart3[8]);
                chart3.Series.Add(seriesChart3[9]);
                chart3.Series.Add(seriesChart3[10]);
                chart3.Series.Add(seriesChart3[11]);
                chart3.Series.Add(seriesChart3[12]);
                chart3.Series.Add(seriesChart3[13]);
                chart3.Series.Add(seriesChart3[14]);
                chart3.Series.Add(seriesChart3[15]);
                chart3.Series.Add(seriesChart3[16]);
                chart3.Series.Add(seriesChart3[17]);

                chart3.Series[0].Points.Add(puntosChart3[0]);
                chart3.Series[1].Points.Add(puntosChart3[1]);
                chart3.Series[2].Points.Add(puntosChart3[2]);
                chart3.Series[3].Points.Add(puntosChart3[3]);
                chart3.Series[4].Points.Add(puntosChart3[4]);
                chart3.Series[5].Points.Add(puntosChart3[5]);
                chart3.Series[6].Points.Add(puntosChart3[6]);
                chart3.Series[7].Points.Add(puntosChart3[7]);
                chart3.Series[8].Points.Add(puntosChart3[8]);
                chart3.Series[9].Points.Add(puntosChart3[9]);
                chart3.Series[10].Points.Add(puntosChart3[10]);
                chart3.Series[11].Points.Add(puntosChart3[11]);
                chart3.Series[12].Points.Add(puntosChart3[12]);
                chart3.Series[13].Points.Add(puntosChart3[13]);
                chart3.Series[14].Points.Add(puntosChart3[14]);
                chart3.Series[15].Points.Add(puntosChart3[15]);
                chart3.Series[16].Points.Add(puntosChart3[16]);
                chart3.Series[17].Points.Add(puntosChart3[17]);

                Color[] colores = new Color[] { Color.DarkOrange };
                chart3.Palette = ChartColorPalette.EarthTones;
            }
            else if (totalDatos3 == 19)
            {
                int total1 = lista3[0].total;
                string estado_civil1 = lista3[0].estado_civil;
                string vivienda1 = lista3[0].vivienda;

                int total2 = lista3[1].total;
                string estado_civil2 = lista3[1].estado_civil;
                string vivienda2 = lista3[1].vivienda;

                int total3 = lista3[2].total;
                string estado_civil3 = lista3[2].estado_civil;
                string vivienda3 = lista3[2].vivienda;

                int total4 = lista3[3].total;
                string estado_civil4 = lista3[3].estado_civil;
                string vivienda4 = lista3[3].vivienda;

                int total5 = lista3[4].total;
                string estado_civil5 = lista3[4].estado_civil;
                string vivienda5 = lista3[4].vivienda;

                int total6 = lista3[5].total;
                string estado_civil6 = lista3[5].estado_civil;
                string vivienda6 = lista3[5].vivienda;

                int total7 = lista3[6].total;
                string estado_civil7 = lista3[6].estado_civil;
                string vivienda7 = lista3[6].vivienda;

                int total8 = lista3[7].total;
                string estado_civil8 = lista3[7].estado_civil;
                string vivienda8 = lista3[7].vivienda;

                int total9 = lista3[8].total;
                string estado_civil9 = lista3[8].estado_civil;
                string vivienda9 = lista3[8].vivienda;

                int total10 = lista3[9].total;
                string estado_civil10 = lista3[9].estado_civil;
                string vivienda10 = lista3[9].vivienda;

                int total11 = lista3[10].total;
                string estado_civil11 = lista3[10].estado_civil;
                string vivienda11 = lista3[10].vivienda;

                int total12 = lista3[11].total;
                string estado_civil12 = lista3[11].estado_civil;
                string vivienda12 = lista3[11].vivienda;

                int total13 = lista3[12].total;
                string estado_civil13 = lista3[12].estado_civil;
                string vivienda13 = lista3[12].vivienda;

                int total14 = lista3[13].total;
                string estado_civil14 = lista3[13].estado_civil;
                string vivienda14 = lista3[13].vivienda;

                int total15 = lista3[14].total;
                string estado_civil15 = lista3[14].estado_civil;
                string vivienda15 = lista3[14].vivienda;

                int total16 = lista3[15].total;
                string estado_civil16 = lista3[15].estado_civil;
                string vivienda16 = lista3[15].vivienda;

                int total17 = lista3[16].total;
                string estado_civil17 = lista3[16].estado_civil;
                string vivienda17 = lista3[16].vivienda;

                int total18 = lista3[17].total;
                string estado_civil18 = lista3[17].estado_civil;
                string vivienda18 = lista3[17].vivienda;

                int total19 = lista3[18].total;
                string estado_civil19 = lista3[18].estado_civil;
                string vivienda19 = lista3[18].vivienda;

                string[] seriesChart3 = { estado_civil1 + "|" + vivienda1 + " = " + total1, estado_civil2 + "|" + vivienda2 + " = " + total2, estado_civil3 + "|" + vivienda3 + " = " + total3, estado_civil4 + "|" + vivienda4 + " = " + total4, estado_civil5 + "|" + vivienda5 + " = " + total5, estado_civil6 + "|" + vivienda6 + " = " + total6, estado_civil7 + "|" + vivienda7 + " = " + total7, estado_civil8 + "|" + vivienda8 + " = " + total8, estado_civil9 + "|" + vivienda9 + " = " + total9, estado_civil10 + "|" + vivienda10 + " = " + total10, estado_civil11 + "|" + vivienda11 + " = " + total11, estado_civil12 + "" + vivienda12 + " = " + total12, estado_civil13 + "|" + vivienda13 + " = " + total13, estado_civil14 + " - " + vivienda14, estado_civil15 + "|" + vivienda15 + " = " + total15, estado_civil16 + "|" + vivienda16 + " = " + total16, estado_civil17 + "|" + vivienda17 + " = " + total17, estado_civil18 + "|" + vivienda18 + " = " + total18, estado_civil19 + "|" + vivienda19 + " = " + total19 };
                int[] puntosChart3 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10, total11, total12, total13, total14, total15, total16, total17, total18,total19 };

                chart3.Titles.Add("Cantidad de personas según con quien viven y la información de donde viven.");

                chart3.Series.Add(seriesChart3[0]);
                chart3.Series.Add(seriesChart3[1]);
                chart3.Series.Add(seriesChart3[2]);
                chart3.Series.Add(seriesChart3[3]);
                chart3.Series.Add(seriesChart3[4]);
                chart3.Series.Add(seriesChart3[5]);
                chart3.Series.Add(seriesChart3[6]);
                chart3.Series.Add(seriesChart3[7]);
                chart3.Series.Add(seriesChart3[8]);
                chart3.Series.Add(seriesChart3[9]);
                chart3.Series.Add(seriesChart3[10]);
                chart3.Series.Add(seriesChart3[11]);
                chart3.Series.Add(seriesChart3[12]);
                chart3.Series.Add(seriesChart3[13]);
                chart3.Series.Add(seriesChart3[14]);
                chart3.Series.Add(seriesChart3[15]);
                chart3.Series.Add(seriesChart3[16]);
                chart3.Series.Add(seriesChart3[17]);
                chart3.Series.Add(seriesChart3[18]);

                chart3.Series[0].Points.Add(puntosChart3[0]);
                chart3.Series[1].Points.Add(puntosChart3[1]);
                chart3.Series[2].Points.Add(puntosChart3[2]);
                chart3.Series[3].Points.Add(puntosChart3[3]);
                chart3.Series[4].Points.Add(puntosChart3[4]);
                chart3.Series[5].Points.Add(puntosChart3[5]);
                chart3.Series[6].Points.Add(puntosChart3[6]);
                chart3.Series[7].Points.Add(puntosChart3[7]);
                chart3.Series[8].Points.Add(puntosChart3[8]);
                chart3.Series[9].Points.Add(puntosChart3[9]);
                chart3.Series[10].Points.Add(puntosChart3[10]);
                chart3.Series[11].Points.Add(puntosChart3[11]);
                chart3.Series[12].Points.Add(puntosChart3[12]);
                chart3.Series[13].Points.Add(puntosChart3[13]);
                chart3.Series[14].Points.Add(puntosChart3[14]);
                chart3.Series[15].Points.Add(puntosChart3[15]);
                chart3.Series[16].Points.Add(puntosChart3[16]);
                chart3.Series[17].Points.Add(puntosChart3[17]);
                chart3.Series[18].Points.Add(puntosChart3[18]);

                Color[] colores = new Color[] { Color.DarkOrange };
                chart3.Palette = ChartColorPalette.EarthTones;
            }

            List<ObjConsulta4> lista4 = conexion.consulta4();
            int totalDatos4 = lista4.Count();
            if (totalDatos4 == 3)
            {
                int total1 = lista4[0].total;
                string sexo1 = lista4[0].sexo;
                string carro1 = lista4[0].carro;

                int total2 = lista4[1].total;
                string sexo2 = lista4[1].sexo;
                string carro2 = lista4[1].carro;

                int total3 = lista4[2].total;
                string sexo3 = lista4[2].sexo;
                string carro3 = lista4[2].carro;

                string[] seriesChart1 = { sexo1 + " - " + carro1, sexo2 + " - " + carro2, sexo3 + " - " + carro3 };
                int[] puntosChart1 = { total1, total2, total3 };

                chart4.Titles.Add("Cantidad de hombres y mujeres que tienen carro propio o no.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart4.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }
            else if (totalDatos4 == 4)
            {
                int total1 = lista4[0].total;
                string sexo1 = lista4[0].sexo;
                string carro1 = lista4[0].carro;

                int total2 = lista4[1].total;
                string sexo2 = lista4[1].sexo;
                string carro2 = lista4[1].carro;

                int total3 = lista4[2].total;
                string sexo3 = lista4[2].sexo;
                string carro3 = lista4[2].carro;

                int total4 = lista4[3].total;
                string sexo4 = lista4[3].sexo;
                string carro4 = lista4[3].carro;

                string[] seriesChart1 = { sexo1 + " - " + carro1, sexo2 + " - " + carro2, sexo3 + " - " + carro3, sexo4 + " - " + carro4 };
                int[] puntosChart1 = { total1, total2, total3, total4 };

                chart4.Titles.Add("Cantidad de hombres y mujeres que tienen carro propio o no.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart4.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }
            else if (totalDatos4 == 5)
            {
                int total1 = lista4[0].total;
                string sexo1 = lista4[0].sexo;
                string carro1 = lista4[0].carro;

                int total2 = lista4[1].total;
                string sexo2 = lista4[1].sexo;
                string carro2 = lista4[1].carro;

                int total3 = lista4[2].total;
                string sexo3 = lista4[2].sexo;
                string carro3 = lista4[2].carro;

                int total4 = lista4[3].total;
                string sexo4 = lista4[3].sexo;
                string carro4 = lista4[3].carro;

                int total5 = lista4[4].total;
                string sexo5 = lista4[4].sexo;
                string carro5 = lista4[4].carro;

                string[] seriesChart1 = { sexo1 + " - " + carro1, sexo2 + " - " + carro2, sexo3 + " - " + carro3, sexo4 + " - " + carro4, sexo5 + " - " + carro5 };
                int[] puntosChart1 = { total1, total2, total3, total4, total5 };

                chart4.Titles.Add("Cantidad de hombres y mujeres que tienen carro propio o no.");

                for (int i = 0; i < seriesChart1.Length; i++)
                {
                    chart4.Series["Series1"].Points.AddXY(seriesChart1[i], puntosChart1[i]);
                }
            }

            List<ObjConsulta5> lista5 = conexion.consulta5();
            int totalDatos5 = lista5.Count();
            if (totalDatos5 == 11)
            {
                int total1 = lista5[0].total;
                string credit1 = lista5[0].credit;
                string estado_civil1 = lista5[0].estado_civil;

                int total2 = lista5[1].total;
                string credit2 = lista5[1].credit;
                string estado_civil2 = lista5[1].estado_civil;

                int total3 = lista5[2].total;
                string credit3 = lista5[2].credit;
                string estado_civil3 = lista5[2].estado_civil;

                int total4 = lista5[3].total;
                string credit4 = lista5[3].credit;
                string estado_civil4 = lista5[3].estado_civil;

                int total5 = lista5[4].total;
                string credit5 = lista5[4].credit;
                string estado_civil5 = lista5[4].estado_civil;

                int total6 = lista5[5].total;
                string credit6 = lista5[5].credit;
                string estado_civil6 = lista5[5].estado_civil;

                int total7 = lista5[6].total;
                string credit7 = lista5[6].credit;
                string estado_civil7 = lista5[6].estado_civil;

                int total8 = lista5[7].total;
                string credit8 = lista5[7].credit;
                string estado_civil8 = lista5[7].estado_civil;

                int total9 = lista5[8].total;
                string credit9 = lista5[8].credit;
                string estado_civil9 = lista5[8].estado_civil;

                int total10 = lista5[9].total;
                string credit10 = lista5[9].credit;
                string estado_civil10 = lista5[9].estado_civil;


                int total11 = lista5[10].total;
                string credit11 = lista5[10].credit;
                string estado_civil11 = lista5[10].estado_civil;

                string[] seriesChart5 = { credit1 + "|" + estado_civil1 + " = " + total1, credit2 + "|" + estado_civil2 + " = " + total2, credit3 + "|" + estado_civil3 + " = " + total3, credit4 + "|" + estado_civil4 + " = " + total4, credit5 + "|" + estado_civil5 + " = " + total5, credit6 + "|" + estado_civil6 + " = " + total6, credit7 + "|" + estado_civil7 + " = " + total7, credit8 + "|" + estado_civil8 + " = " + total8, credit9 + "|" + estado_civil9 + " = " + total9, credit10 + "|" + estado_civil10 + " = " + total10, credit11 + "|" + estado_civil11 + " = " + total11};
                int[] puntosChart5 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10, total11};

                chart5.Titles.Add("Cantidad de estado de los créditos según el estado civil");

                chart5.Series.Add(seriesChart5[0]);
                chart5.Series.Add(seriesChart5[1]);
                chart5.Series.Add(seriesChart5[2]);
                chart5.Series.Add(seriesChart5[3]);
                chart5.Series.Add(seriesChart5[4]);
                chart5.Series.Add(seriesChart5[5]);
                chart5.Series.Add(seriesChart5[6]);
                chart5.Series.Add(seriesChart5[7]);
                chart5.Series.Add(seriesChart5[8]);
                chart5.Series.Add(seriesChart5[9]);
                chart5.Series.Add(seriesChart5[10]);

                chart5.Series[0].Points.Add(puntosChart5[0]);
                chart5.Series[1].Points.Add(puntosChart5[1]);
                chart5.Series[2].Points.Add(puntosChart5[2]);
                chart5.Series[3].Points.Add(puntosChart5[3]);
                chart5.Series[4].Points.Add(puntosChart5[4]);
                chart5.Series[5].Points.Add(puntosChart5[5]);
                chart5.Series[6].Points.Add(puntosChart5[6]);
                chart5.Series[7].Points.Add(puntosChart5[7]);
                chart5.Series[8].Points.Add(puntosChart5[8]);
                chart5.Series[9].Points.Add(puntosChart5[9]);
                chart5.Series[10].Points.Add(puntosChart5[10]);

                Color[] colores = new Color[] { Color.DarkOrange };
                chart5.Palette = ChartColorPalette.EarthTones;
            }
            else if (totalDatos5 == 12)
            {
                int total1 = lista5[0].total;
                string credit1 = lista5[0].credit;
                string estado_civil1 = lista5[0].estado_civil;

                int total2 = lista5[1].total;
                string credit2 = lista5[1].credit;
                string estado_civil2 = lista5[1].estado_civil;

                int total3 = lista5[2].total;
                string credit3 = lista5[2].credit;
                string estado_civil3 = lista5[2].estado_civil;

                int total4 = lista5[3].total;
                string credit4 = lista5[3].credit;
                string estado_civil4 = lista5[3].estado_civil;

                int total5 = lista5[4].total;
                string credit5 = lista5[4].credit;
                string estado_civil5 = lista5[4].estado_civil;

                int total6 = lista5[5].total;
                string credit6 = lista5[5].credit;
                string estado_civil6 = lista5[5].estado_civil;

                int total7 = lista5[6].total;
                string credit7 = lista5[6].credit;
                string estado_civil7 = lista5[6].estado_civil;

                int total8 = lista5[7].total;
                string credit8 = lista5[7].credit;
                string estado_civil8 = lista5[7].estado_civil;

                int total9 = lista5[8].total;
                string credit9 = lista5[8].credit;
                string estado_civil9 = lista5[8].estado_civil;

                int total10 = lista5[9].total;
                string credit10 = lista5[9].credit;
                string estado_civil10 = lista5[9].estado_civil;

                int total11 = lista5[10].total;
                string credit11 = lista5[10].credit;
                string estado_civil11 = lista5[10].estado_civil;

                int total12 = lista5[11].total;
                string credit12 = lista5[11].credit;
                string estado_civil12 = lista5[11].estado_civil;

                string[] seriesChart5 = { credit1 + "|" + estado_civil1 + " = " + total1, credit2 + "|" + estado_civil2 + " = " + total2, credit3 + "|" + estado_civil3 + " = " + total3, credit4 + "|" + estado_civil4 + " = " + total4, credit5 + "|" + estado_civil5 + " = " + total5, credit6 + "|" + estado_civil6 + " = " + total6, credit7 + "|" + estado_civil7 + " = " + total7, credit8 + "|" + estado_civil8 + " = " + total8, credit9 + "|" + estado_civil9 + " = " + total9, credit10 + "|" + estado_civil10 + " = " + total10, credit11 + "|" + estado_civil11 + " = " + total11, credit12 + "|" + estado_civil12 + " = " + total12};
                int[] puntosChart5 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10, total11, total12};

                chart5.Titles.Add("Cantidad de estado de los créditos según el estado civil");

                chart5.Series.Add(seriesChart5[0]);
                chart5.Series.Add(seriesChart5[1]);
                chart5.Series.Add(seriesChart5[2]);
                chart5.Series.Add(seriesChart5[3]);
                chart5.Series.Add(seriesChart5[4]);
                chart5.Series.Add(seriesChart5[5]);
                chart5.Series.Add(seriesChart5[6]);
                chart5.Series.Add(seriesChart5[7]);
                chart5.Series.Add(seriesChart5[8]);
                chart5.Series.Add(seriesChart5[9]);
                chart5.Series.Add(seriesChart5[10]);
                chart5.Series.Add(seriesChart5[11]);

                chart5.Series[0].Points.Add(puntosChart5[0]);
                chart5.Series[1].Points.Add(puntosChart5[1]);
                chart5.Series[2].Points.Add(puntosChart5[2]);
                chart5.Series[3].Points.Add(puntosChart5[3]);
                chart5.Series[4].Points.Add(puntosChart5[4]);
                chart5.Series[5].Points.Add(puntosChart5[5]);
                chart5.Series[6].Points.Add(puntosChart5[6]);
                chart5.Series[7].Points.Add(puntosChart5[7]);
                chart5.Series[8].Points.Add(puntosChart5[8]);
                chart5.Series[9].Points.Add(puntosChart5[9]);
                chart5.Series[10].Points.Add(puntosChart5[10]);
                chart5.Series[11].Points.Add(puntosChart5[11]);

                Color[] colores = new Color[] { Color.DarkOrange };
                chart5.Palette = ChartColorPalette.EarthTones;
            }
            else if (totalDatos5 == 13)
            {
                int total1 = lista5[0].total;
                string credit1 = lista5[0].credit;
                string estado_civil1 = lista5[0].estado_civil;

                int total2 = lista5[1].total;
                string credit2 = lista5[1].credit;
                string estado_civil2 = lista5[1].estado_civil;

                int total3 = lista5[2].total;
                string credit3 = lista5[2].credit;
                string estado_civil3 = lista5[2].estado_civil;

                int total4 = lista5[3].total;
                string credit4 = lista5[3].credit;
                string estado_civil4 = lista5[3].estado_civil;

                int total5 = lista5[4].total;
                string credit5 = lista5[4].credit;
                string estado_civil5 = lista5[4].estado_civil;

                int total6 = lista5[5].total;
                string credit6 = lista5[5].credit;
                string estado_civil6 = lista5[5].estado_civil;

                int total7 = lista5[6].total;
                string credit7 = lista5[6].credit;
                string estado_civil7 = lista5[6].estado_civil;

                int total8 = lista5[7].total;
                string credit8 = lista5[7].credit;
                string estado_civil8 = lista5[7].estado_civil;

                int total9 = lista5[8].total;
                string credit9 = lista5[8].credit;
                string estado_civil9 = lista5[8].estado_civil;

                int total10 = lista5[9].total;
                string credit10 = lista5[9].credit;
                string estado_civil10 = lista5[9].estado_civil;

                int total11 = lista5[10].total;
                string credit11 = lista5[10].credit;
                string estado_civil11 = lista5[10].estado_civil;

                int total12 = lista5[11].total;
                string credit12 = lista5[11].credit;
                string estado_civil12 = lista5[11].estado_civil;

                int total13 = lista5[12].total;
                string credit13 = lista5[12].credit;
                string estado_civil13 = lista5[12].estado_civil;

                string[] seriesChart5 = { credit1 + "|" + estado_civil1 + " = " + total1, credit2 + "|" + estado_civil2 + " = " + total2, credit3 + "|" + estado_civil3 + " = " + total3, credit4 + "|" + estado_civil4 + " = " + total4, credit5 + "|" + estado_civil5 + " = " + total5, credit6 + "|" + estado_civil6 + " = " + total6, credit7 + "|" + estado_civil7 + " = " + total7, credit8 + "|" + estado_civil8 + " = " + total8, credit9 + "|" + estado_civil9 + " = " + total9, credit10 + "|" + estado_civil10 + " = " + total10, credit11 + "|" + estado_civil11 + " = " + total11, credit12 + "|" + estado_civil12 + " = " + total12, credit13 + "|" + estado_civil13 + " = " + total13};
                int[] puntosChart5 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10, total11, total12, total13 };

                cchart5.Titles.Add("Cantidad de estado de los créditos según el estado civil");

                chart5.Series.Add(seriesChart5[0]);
                chart5.Series.Add(seriesChart5[1]);
                chart5.Series.Add(seriesChart5[2]);
                chart5.Series.Add(seriesChart5[3]);
                chart5.Series.Add(seriesChart5[4]);
                chart5.Series.Add(seriesChart5[5]);
                chart5.Series.Add(seriesChart5[6]);
                chart5.Series.Add(seriesChart5[7]);
                chart5.Series.Add(seriesChart5[8]);
                chart5.Series.Add(seriesChart5[9]);
                chart5.Series.Add(seriesChart5[10]);
                chart5.Series.Add(seriesChart5[11]);
                chart5.Series.Add(seriesChart5[12]);

                chart5.Series[0].Points.Add(puntosChart5[0]);
                chart5.Series[1].Points.Add(puntosChart5[1]);
                chart5.Series[2].Points.Add(puntosChart5[2]);
                chart5.Series[3].Points.Add(puntosChart5[3]);
                chart5.Series[4].Points.Add(puntosChart5[4]);
                chart5.Series[5].Points.Add(puntosChart5[5]);
                chart5.Series[6].Points.Add(puntosChart5[6]);
                chart5.Series[7].Points.Add(puntosChart5[7]);
                chart5.Series[8].Points.Add(puntosChart5[8]);
                chart5.Series[9].Points.Add(puntosChart5[9]);
                chart5.Series[10].Points.Add(puntosChart5[10]);
                chart5.Series[11].Points.Add(puntosChart5[11]);
                chart5.Series[12].Points.Add(puntosChart5[12]);

                Color[] colores = new Color[] { Color.DarkOrange };
                chart5.Palette = ChartColorPalette.EarthTones;
            }
            else if (totalDatos5 == 14)
            {
                int total1 = lista5[0].total;
                string credit1 = lista5[0].credit;
                string estado_civil1 = lista5[0].estado_civil;

                int total2 = lista5[1].total;
                string credit2 = lista5[1].credit;
                string estado_civil2 = lista5[1].estado_civil;

                int total3 = lista5[2].total;
                string credit3 = lista5[2].credit;
                string estado_civil3 = lista5[2].estado_civil;

                int total4 = lista5[3].total;
                string credit4 = lista5[3].credit;
                string estado_civil4 = lista5[3].estado_civil;

                int total5 = lista5[4].total;
                string credit5 = lista5[4].credit;
                string estado_civil5 = lista5[4].estado_civil;

                int total6 = lista5[5].total;
                string credit6 = lista5[5].credit;
                string estado_civil6 = lista5[5].estado_civil;

                int total7 = lista5[6].total;
                string credit7 = lista5[6].credit;
                string estado_civil7 = lista5[6].estado_civil;

                int total8 = lista5[7].total;
                string credit8 = lista5[7].credit;
                string estado_civil8 = lista5[7].estado_civil;

                int total9 = lista5[8].total;
                string credit9 = lista5[8].credit;
                string estado_civil9 = lista5[8].estado_civil;

                int total10 = lista5[9].total;
                string credit10 = lista5[9].credit;
                string estado_civil10 = lista5[9].estado_civil;

                int total11 = lista5[10].total;
                string credit11 = lista5[10].credit;
                string estado_civil11 = lista5[10].estado_civil;

                int total12 = lista5[11].total;
                string credit12 = lista5[11].credit;
                string estado_civil12 = lista5[11].estado_civil;

                int total13 = lista5[12].total;
                string credit13 = lista5[12].credit;
                string estado_civil13 = lista5[12].estado_civil;

                int total14 = lista5[13].total;
                string credit14 = lista5[13].credit;
                string estado_civil14 = lista5[13].estado_civil;

                string[] seriesChart5 = { credit1 + "|" + estado_civil1 + " = " + total1, credit2 + "|" + estado_civil2 + " = " + total2, credit3 + "|" + estado_civil3 + " = " + total3, credit4 + "|" + estado_civil4 + " = " + total4, credit5 + "|" + estado_civil5 + " = " + total5, credit6 + "|" + estado_civil6 + " = " + total6, credit7 + "|" + estado_civil7 + " = " + total7, credit8 + "|" + estado_civil8 + " = " + total8, credit9 + "|" + estado_civil9 + " = " + total9, credit10 + "|" + estado_civil10 + " = " + total10, credit11 + "|" + estado_civil11 + " = " + total11, credit12 + "|" + estado_civil12 + " = " + total12, credit13 + "|" + estado_civil13 + " = " + total13, credit14 + "|" + estado_civil14 + " = " + total14 };
                int[] puntosChart5 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10, total11, total12, total13, total14 };

                chart5.Titles.Add("Cantidad de estado de los créditos según el estado civil");

                chart5.Series.Add(seriesChart5[0]);
                chart5.Series.Add(seriesChart5[1]);
                chart5.Series.Add(seriesChart5[2]);
                chart5.Series.Add(seriesChart5[3]);
                chart5.Series.Add(seriesChart5[4]);
                chart5.Series.Add(seriesChart5[5]);
                chart5.Series.Add(seriesChart5[6]);
                chart5.Series.Add(seriesChart5[7]);
                chart5.Series.Add(seriesChart5[8]);
                chart5.Series.Add(seriesChart5[9]);
                chart5.Series.Add(seriesChart5[10]);
                chart5.Series.Add(seriesChart5[11]);
                chart5.Series.Add(seriesChart5[12]);
                chart5.Series.Add(seriesChart5[13]);

                chart5.Series[0].Points.Add(puntosChart5[0]);
                chart5.Series[1].Points.Add(puntosChart5[1]);
                chart5.Series[2].Points.Add(puntosChart5[2]);
                chart5.Series[3].Points.Add(puntosChart5[3]);
                chart5.Series[4].Points.Add(puntosChart5[4]);
                chart5.Series[5].Points.Add(puntosChart5[5]);
                chart5.Series[6].Points.Add(puntosChart5[6]);
                chart5.Series[7].Points.Add(puntosChart5[7]);
                chart5.Series[8].Points.Add(puntosChart5[8]);
                chart5.Series[9].Points.Add(puntosChart5[9]);
                chart5.Series[10].Points.Add(puntosChart5[10]);
                chart5.Series[11].Points.Add(puntosChart5[11]);
                chart5.Series[12].Points.Add(puntosChart5[12]);
                chart5.Series[13].Points.Add(puntosChart5[13]);

                Color[] colores = new Color[] { Color.DarkOrange };
                chart5.Palette = ChartColorPalette.EarthTones;
            }
            else if (totalDatos5 == 15)
            {
                int total1 = lista5[0].total;
                string credit1 = lista5[0].credit;
                string estado_civil1 = lista5[0].estado_civil;

                int total2 = lista5[1].total;
                string credit2 = lista5[1].credit;
                string estado_civil2 = lista5[1].estado_civil;

                int total3 = lista5[2].total;
                string credit3 = lista5[2].credit;
                string estado_civil3 = lista5[2].estado_civil;

                int total4 = lista5[3].total;
                string credit4 = lista5[3].credit;
                string estado_civil4 = lista5[3].estado_civil;

                int total5 = lista5[4].total;
                string credit5 = lista5[4].credit;
                string estado_civil5 = lista5[4].estado_civil;

                int total6 = lista5[5].total;
                string credit6 = lista5[5].credit;
                string estado_civil6 = lista5[5].estado_civil;

                int total7 = lista5[6].total;
                string credit7 = lista5[6].credit;
                string estado_civil7 = lista5[6].estado_civil;

                int total8 = lista5[7].total;
                string credit8 = lista5[7].credit;
                string estado_civil8 = lista5[7].estado_civil;

                int total9 = lista5[8].total;
                string credit9 = lista5[8].credit;
                string estado_civil9 = lista5[8].estado_civil;

                int total10 = lista5[9].total;
                string credit10 = lista5[9].credit;
                string estado_civil10 = lista5[9].estado_civil;

                int total11 = lista5[10].total;
                string credit11 = lista5[10].credit;
                string estado_civil11 = lista5[10].estado_civil;

                int total12 = lista5[11].total;
                string credit12 = lista5[11].credit;
                string estado_civil12 = lista5[11].estado_civil;

                int total13 = lista5[12].total;
                string credit13 = lista5[12].credit;
                string estado_civil13 = lista5[12].estado_civil;

                int total14 = lista5[13].total;
                string credit14 = lista5[13].credit;
                string estado_civil14 = lista5[13].estado_civil;

                int total15 = lista5[14].total;
                string credit15 = lista5[14].credit;
                string estado_civil15 = lista5[14].estado_civil;

                string[] seriesChart5 = { credit1 + "|" + estado_civil1 + " = " + total1, credit2 + "|" + estado_civil2 + " = " + total2, credit3 + "|" + estado_civil3 + " = " + total3, credit4 + "|" + estado_civil4 + " = " + total4, credit5 + "|" + estado_civil5 + " = " + total5, credit6 + "|" + estado_civil6 + " = " + total6, credit7 + "|" + estado_civil7 + " = " + total7, credit8 + "|" + estado_civil8 + " = " + total8, credit9 + "|" + estado_civil9 + " = " + total9, credit10 + "|" + estado_civil10 + " = " + total10, credit11 + "|" + estado_civil11 + " = " + total11, credit12 + "|" + estado_civil12 + " = " + total12, credit13 + "|" + estado_civil13 + " = " + total13, credit14 + "|" + estado_civil14 + " = " + total14, credit15 + "|" + estado_civil15 + " = " + total15 };
                int[] puntosChart5 = { total1, total2, total3, total4, total5, total6, total7, total8, total9, total10, total11, total12, total13, total14, total15 };

                chart5.Titles.Add("Cantidad de estado de los créditos según el estado civil");
                chart5.Series.Add(seriesChart5[0]);
                chart5.Series.Add(seriesChart5[1]);
                chart5.Series.Add(seriesChart5[2]);
                chart5.Series.Add(seriesChart5[3]);
                chart5.Series.Add(seriesChart5[4]);
                chart5.Series.Add(seriesChart5[5]);
                chart5.Series.Add(seriesChart5[6]);
                chart5.Series.Add(seriesChart5[7]);
                chart5.Series.Add(seriesChart5[8]);
                chart5.Series.Add(seriesChart5[9]);
                chart5.Series.Add(seriesChart5[10]);
                chart5.Series.Add(seriesChart5[11]);
                chart5.Series.Add(seriesChart5[12]);
                chart5.Series.Add(seriesChart5[13]);
                chart5.Series.Add(seriesChart5[14]);

                chart5.Series[0].Points.Add(puntosChart5[0]);
                chart5.Series[1].Points.Add(puntosChart5[1]);
                chart5.Series[2].Points.Add(puntosChart5[2]);
                chart5.Series[3].Points.Add(puntosChart5[3]);
                chart5.Series[4].Points.Add(puntosChart5[4]);
                chart5.Series[5].Points.Add(puntosChart5[5]);
                chart5.Series[6].Points.Add(puntosChart5[6]);
                chart5.Series[7].Points.Add(puntosChart5[7]);
                chart5.Series[8].Points.Add(puntosChart5[8]);
                chart5.Series[9].Points.Add(puntosChart5[9]);
                chart5.Series[10].Points.Add(puntosChart5[10]);
                chart5.Series[11].Points.Add(puntosChart5[11]);
                chart5.Series[12].Points.Add(puntosChart5[12]);
                chart5.Series[13].Points.Add(puntosChart5[13]);
                chart5.Series[14].Points.Add(puntosChart5[14]);

                Color[] colores = new Color[] { Color.DarkOrange };
                chart5.Palette = ChartColorPalette.EarthTones;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form formulario = new Form1();
            formulario.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form formulario = new Form1();
            formulario.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cargarGraficos();
        }
    }
}
